package com.ernok.cerberus;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

import com.ernok.ima.ImaFileRequest;
import com.ernok.ima.ImaProtocol;
import com.ernok.ima.ImaRequest;

public abstract class ServerListener implements AutoCloseable {
	ServerSocket socket;

	static final int BUFFER_SIZE = 1024 * 1024;

	public ServerListener(ServerSocket socket) {
		this.socket = socket;
	}

	public void close() throws Exception {
		socket.close();
	}
	
	public abstract void onConnect(Socket client);

	public void listen() throws IOException {
		while (true) {
			// wait for next client
			Socket client;
			try {
				client = socket.accept();
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}

			// start connection with the new client in a new thread
			Thread thread = new Thread(() -> this.onConnect(client));

			thread.start();
		}
	}

}
