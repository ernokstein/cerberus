package com.ernok.cerberus;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class Cerberus {
	public static void main(String[] args) {
		int serverPort = 9494;

		// abrir socket de server
		try (ServerSocket socket = new ServerSocket(serverPort)) {
			// instanciador servidor en ese socket
			try (CerberusListener listener = new CerberusListener(socket, "./site")) {
				// pararse a escuchar...
				System.out.println("Started listening on " + socket.getLocalSocketAddress());
				listener.listen();
				System.out.println("Ended listening on " + socket.getLocalSocketAddress());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
