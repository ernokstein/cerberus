package com.ernok.cerberus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

import com.ernok.ima.ImaFileRequest;
import com.ernok.ima.ImaProtocol;
import com.ernok.ima.ImaRequest;

public class CerberusListener extends ServerListener {
	String sitePath;

	public CerberusListener(ServerSocket socket, String sitePath) {
		super(socket);
		this.sitePath = sitePath;
	}

	public String loadFile(String filePath) {
		// load file
		try {
			File file = new File(sitePath + filePath);
			StringBuilder sb = new StringBuilder();
			try (Scanner scanner = new Scanner(file);) {
				while (scanner.hasNextLine()) {
					sb.append(scanner.nextLine());
				}
			}
			return sb.toString();
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	private void sendFile(Socket client, String filePath) throws IOException {
		// load file
		String fileContent = loadFile(filePath);

		// send file
		if (fileContent == null) {
			ImaProtocol.sendError(client, "File not found");
		} else {
			ImaProtocol.sendFile(client, fileContent);
		}
	}

	@Override
	public void onConnect(Socket client) {
		System.out.println("New connection with " + client.getRemoteSocketAddress());

		try {
			ImaRequest request = ImaProtocol.listenRequest(client);
			if (request != null) {
				switch (request.getType()) {
					case File: {
						sendFile(client, ((ImaFileRequest) request).path);
						break;
					}

					default: {
						throw new RuntimeException("Unimplemented request type " + request.getType().toString());
					}
				}
			}
		} catch (SocketException se) {

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Ended connection with " + client.getRemoteSocketAddress());
	}
}
